# Parallax Background

This a simple module that allows to set a vertical
[Parallax effect](https://ianlunn.co.uk/plugins/jquery-parallax/) on the background of any element on the DOM.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/parallax_bg).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/parallax_bg).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Goto **Admin / Structure / Parallax elements**
- Add new element you want to apply the Parallax effect using any valid jQuery selector. The selector should point to the element that holds the background, for example: `#top-content`, `body.one-page #super-banner`


## Maintainers

- Alberto Siles - [hatuhay](https://www.drupal.org/u/hatuhay)
- Sándor Juhász - [lonalore](https://www.drupal.org/u/lonalore)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
